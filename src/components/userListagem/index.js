import React, { Component } from "react";
import ReactTable from "react-table";
import { Link } from "react-router-dom";
import moment from "moment";
import 'moment/locale/pt-br';

export default class UserListagem extends Component {
    constructor(props) {
        super(props);

        this.state = { data: [] };

        this.listUsers();
    }

    listUsers = () => {
        const requestOptions = {
            method: 'GET',
        };

        fetch(`http://54.147.244.100/api/customers`, requestOptions)
            .then(response => response.json())
            .then(data => {
                const _data = data.data;

                _data.forEach(item => {
                    this.setState({
                        data: [{
                            id: item.id,
                            editar: item.id,
                            visualizar: item.id,
                            nome: item.name,
                            cpf: this.cpf_mask(item.cpf),
                            dob: moment(item.birthdate).format('L'),
                        }].concat(this.state.data)
                    });
                });
            })
            .catch(error => { /*alert(error)*/ })
    }

    cpf_mask = (cpf) => {
        cpf = cpf.replace(/\D/g,"");
        cpf = cpf.replace(/(\d{3})(\d)/,"$1.$2");
        cpf = cpf.replace(/(\d{3})(\d)/,"$1.$2");
        cpf = cpf.replace(/(\d{3})(\d{1,2})$/,"$1-$2");

        return cpf;
    }

    handleDelete = (e) => {

        console.log('aqui');

            // axios.post(`http://54.147.244.100/api/customers`, JSON.stringify(data), { headers: {
            //     'Content-Type': 'application/json'
            // }})
            //     .then(data => {
            //         if (data.errors) {
            //             this.showErrors(data.errors)
            //         } else {
            //             this.setState({
            //                 name: '',
            //                 icon: '',
            //                 status: ''
            //             });
            //             this.setState({
            //                 submitted: false
            //             });
            //             alert('Cliente criado.')
            //         }
            //     })
            //     .catch(error => {
            //         alert(error);
            //     });
        
    }

    render() {
        const {data} = this.state

        const columns = [
            {
                Header: "ID",
                accessor: "id",
                maxWidth: 60
            },
            {
                Header: "Nome",
                accessor: "nome"
            },
            {
                Header: "CPF",
                accessor: "cpf"
            },
            {
                Header: "Data de Nascimento",
                accessor: "dob"
            },
            {
                Header: "",
                accessor: "visualizar",
                maxWidth: 90,
                Cell: props => (
                    <Link to={`/view/${props.value}`}>
                        <button>
                            <span>Visualizar</span>
                        </button>
                    </Link>
                )
            },
            {
                Header: "",
                accessor: "editar",
                maxWidth: 90,
                Cell: props => (
                    <Link to={`/edit/${props.value}`}>
                        <button>
                            <span>Editar</span>
                        </button>
                    </Link>
                )
            }
        ];
        return (
            <div className="main-cont">
                <ReactTable
                    data={data}
                    columns={columns}
                    loading={false}
                    minRows={1}
                    className="-striped -highlight"
                />
            </div>
        );
    }
}
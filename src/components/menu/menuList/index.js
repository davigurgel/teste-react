import React, { Component } from 'react';
import MenuHeader from '../menuHeader';
import { Link } from "react-router-dom";


export default class MenuList extends Component {
    render() {
        return (
            <div className="menu-list">
                <MenuHeader />

                <div className="menu-itens">
                    <Link to={"/"}>
                        <li className="menu-item">
                            Listagem
                        </li>
                    </Link>
                    <Link to={"/create"}>
                        <li className="menu-item">
                            Criar
                        </li>
                    </Link>
                </div>

            </div>
        )
    }
}

import React, { Component, Fragment } from "react";
import axios from "axios";
import moment from "moment";
import 'moment/locale/pt-br';
import InputMask from "react-input-mask";

export default class UserView extends Component {
    constructor(props) {
        super(props);

        const { match: { params } } = this.props;

        const requestOptions = { method: "GET" };

        this.state = {
            id: '',
            name: '',
            cpf: '',
            dob: ''
        };

        fetch(`http://54.147.244.100/api/customers/${params.id}`, requestOptions)
            .then(response => response.json())
            .then(data => {
                this.setState({ id: data.id, name: data.name, cpf: data.cpf, dob: moment(data.birthdate).format('L') });
            });

    }

    handleChange = (e) => {

        const { name, value } = e.target

        this.setState({ [name]: value })
    }

    showErrors = (errors, redirect = null) => {
        const errorsKeys = Object.keys(errors);
        let messages = '';

        for (const key of errorsKeys) {
            messages = messages + ' ' + errors[key] + '\n';
        }

        alert(messages);
    }

    removeMask = input => {
        return input.replace('.', '').replace('-', '').replace(' ', '').replace('.', '').replace(' ', '')
    }

    formatDatePattern = date => {
        date = date.split('/')
        let day = date[0]
        let month = date[1]
        let year = date[2]
        return `${year}-${month}-${day}`
    }

    handleDelete = () => {
        const { match: { params } } = this.props;
        if(window.confirm("Tem certeza que deseja excluir esse cliente?")){

            const requestOptions = { method: "DELETE" };

            fetch(`http://54.147.244.100/api/customers/${params.id}`, requestOptions)
                .then(response => this.props.history.push("/"))
                .catch(error => {
                    alert(error);
                });;
        }

    }

    closeAlert = (alert) => {
        this.setState({ alert: { ...this.state.alert, show: false } })

        if (alert.redirect) {
            this.props.history.push(alert.redirect);
        }
    }

    validateCpf = (cpf) => {
        cpf = cpf.replace(/[^\d]+/g,'');
        let numeros, digitos, soma, i, resultado, digitos_iguais;
        digitos_iguais = 1;
        if (cpf.length < 11){
            return false;
        }
        for (let i = 0; i < cpf.length - 1; i++){
            if (cpf.charAt(i) != cpf.charAt(i + 1)){
                digitos_iguais = 0;
                break;
            }
        }
        if (!digitos_iguais){
            numeros = cpf.substring(0,9);
            digitos = cpf.substring(9);
            soma = 0;
            for (let i = 10; i > 1; i--) {
                soma += numeros.charAt(10 - i) * i;
            }
            resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
            if (resultado != digitos.charAt(0)){
                return false;
            }
            numeros = cpf.substring(0,10);
            soma = 0;
            for (let i = 11; i > 1; i--) {
                soma += numeros.charAt(11 - i) * i;                
            }
            resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
            if (resultado != digitos.charAt(1)) {
                return  false;  
            }

            return true;
        } else {
            return false;
        }
    }

    render() {
        const { name, cpf, dob, submitted } = this.state;

        return (
            <div className="conteudo-geral">
                <div className="conteudo-formulario">
                    <div className="titulo-mall">Clientes</div>
                    <div className="inputs-container">
                        <div className="input-item">
                            <label htmlFor="">Nome:</label>
                            <input disabled='disabled' type="text" name="name" value={name} onChange={this.handleChange} />
                            {submitted && !name && 
                                <div className="help-block">O campo Nome é obrigatório.</div>
                            }
                        </div>
                        <div className="input-item">
                            <label htmlFor="">CPF:</label>
                            <InputMask disabled='disabled' name="cpf" mask="999.999.999-99" value={cpf} onChange={this.handleChange} />
                            {submitted && !cpf &&
                                <div className="help-block">Insira o CPF do usuário.</div>
                            }

                            {submitted && !this.validateCpf(cpf) &&
                                <div className="help-block">Insira um CPF válido.</div>
                            }
                        </div>
                        <div className="input-item">
                            <label htmlFor="">Data de nascimento:</label>
                            <InputMask disabled='disabled' name="dob" mask="99/99/9999" value={dob} onChange={this.handleChange} />
                            {submitted && !dob &&
                                <div className="help-block">O campo Data de Nascimento é obrigatório.</div>
                            }
                        </div>
                    </div>
                </div>

                <div className="botao-apagar" onClick={this.handleDelete}>Apagar</div>

            </div>
            
        );
    }
}
import React from 'react';
import { render } from 'react-dom';
import { App } from './containers/App/App';
import './styles.css';


render(
	<App />,
	document.getElementById('app')
);


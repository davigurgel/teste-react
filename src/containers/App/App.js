import React, { Component, Fragment } from "react";
import {BrowserRouter, Route, Switch } from "react-router-dom";
import UserListagem from "../../components/userListagem";
import UserCreate from "../../components/userCreate";
import UserView from "../../components/userView";
import UserEdit from "../../components/userEdit";
import MenuList from "../../components/menu/menuList";

class App extends Component {
    constructor(props) {
        super(props);

        const { dispatch } = this.props;
        this.state = {
            menuLeft: false,
            menuRight: false
        };
    } 

    render() {
        const { alert } = this.props;

        return (
            <div>
              <BrowserRouter>
                  <Switch>
                      <Fragment>
                            <div className="main">
                                <div>
                                    <MenuList />
                                </div>
                                <div className="main-right">
                                  <Route exact path="/" component={UserListagem} />
                                </div>
                                <div className="main-right">
                                  <Route exact path="/create" component={UserCreate} />
                                </div>
                                <div className="main-right">
                                  <Route exact path="/view/:id" component={UserView} />
                                </div>
                                <div className="main-right">
                                  <Route exact path="/edit/:id" component={UserEdit} />
                                </div>
                            </div>
                      </Fragment>
                  </Switch>
              </BrowserRouter>
            </div>
        );
    }
}

export { App };
